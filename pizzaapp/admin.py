from django.contrib import admin

from .models import Store, StoreType

admin.site.register(Store)
admin.site.register(StoreType)