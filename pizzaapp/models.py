from django.db import models

class BaseModel(models.Model):
    Created = models.DateTimeField(auto_now_add=True)
    LastModified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class StoreType(BaseModel):
    Description = models.CharField(max_length=50)
    Abbreviation = models.CharField(max_length=1, unique=True)


class Store(BaseModel):
    Number = models.PositiveIntegerField(null=False, unique=True)
    Description = models.CharField(max_length=200)
    StoreType = models.ForeignKey(StoreType, on_delete=models.CASCADE)
